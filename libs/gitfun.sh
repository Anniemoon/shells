#!/bin/sh
# Author: Annie Dong
# git functions
# Install git
# add gitName='annie' to ~/.shells/.shellconfig

# git checkout new branch

source ~/.shells/.shellconfig

gitnb(){
  branchName=""
  info="${gitName}-$(date +%Y%m%d)"
  echo 'git new branch...'
  echo 'sprint number:'
  read sprintNum
  echo 'issue number: (VEB-1234...)'
  read issueNum
  echo 'description:'
  read des

  if [ -n "$sprintNum" ]
    then
    echo $sprintNum
    branchName="${branchName}sprint${sprintNum}-"
  fi
  if [ -n "$issueNum" ]
   then
    branchName="${branchName}${issueNum}-"
  fi
  if [ -n "$des" ]
    then
    branchName="${branchName}${info}-${des}"
  else
    branchName="${branchName}${info}"
  fi

  git checkout -b $branchName
}


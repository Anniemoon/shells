#!/bin/sh
# Author: Annie Dong
# sshpass servers
. ~/.shells/libs/color.sh

vebenvs(){
  envs=('qa' 'stage' 'perf' 'pro')
  echo "VEB environments list: ${RED}${envs[@]}${NC}"
}

vebqa(){
  list=(
  '10.219.4.241 web01'
  '10.219.4.242 web02'
  '10.219.34.242 worker01'
  '10.219.34.243 worker02'
  '10.219.34.240 redis'
  )

  echo "${RED}=== QA Environments ===${NC}"
  for value in "${list[@]}"
  do
    echo "${value}"
  done
  echo "${RED}========================${NC}"
}

vebstage(){
  list=(
  '10.220.129.241 web01'
  '10.220.129.242 web02'
  '10.220.141.242 worker01'
  '10.220.141.243 worker02'
  '10.220.141.240 redis'
  )

  echo "${RED}=== STAGE Environments ===${NC}"
  for value in "${list[@]}"
  do
    echo "${value}"
  done
  echo "${RED}==========================${NC}"
}

vebperf(){
  list=(
  '10.119.140.50 web01'
  '10.119.140.51 web02'
  '10.119.140.52 web03'
  '10.119.140.53 web04'
  '10.119.140.54 web05'
  '10.119.140.55 web06'
  '10.119.140.56 web07'
  '10.119.140.57 web08'
  '10.119.140.58 web09'
  '10.119.140.59 web10'
  '10.119.163.34 worker01'
  '10.119.163.35 worker02'
  '10.119.163.32 redis01'
  '10.119.163.33 redis02'
  )

  echo "${RED}=== PERF environments ===${NC}"
  for value in "${list[@]}"
  do
    echo "${value}"
  done
  echo "${RED}=========================${NC}"
}

vebpro(){
  list=(
  '10.119.138.221 web01'
  '10.119.138.222 web02'
  '10.119.138.223 web03'
  '10.119.138.224 web04'
  '10.119.138.225 web05'
  '10.119.138.226 web06'
  '10.119.138.227 web07'
  '10.119.138.228 web08'
  '10.119.138.218 web09'
  '10.119.138.219 web10'
  '10.119.162.242 worker01'
  '10.119.162.243 worker02'
  '10.119.162.240 redis01'
  '10.119.162.241 redis02'
  )

  echo "${RED}=== PRO environments ===${NC}"
  for value in "${list[@]}"
  do
    echo "${value}"
  done
  echo "${RED}=========================${NC}"
}

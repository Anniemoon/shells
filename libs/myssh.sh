#!/bin/sh
. ~/.shells/libs/color.sh
. ~/.shells/.shellconfig

myssh(){
  echo "${RED}ssh $1 -l $sshUsername...${NC}"
  sshpass -p "$sshPassword" ssh -o StrictHostKeyChecking=no "$1" -l "$sshUsername"
}


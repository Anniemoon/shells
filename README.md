## Git functions
command： `gitnb`

## SSH&LIST
command: `sshveb`
         `vlist`

### Steps:

Author: Annie Dong
 1. `git clone https://gitee.com/Anniemoon/shells.git ~/.shells`
 2. **Install sshpass**
 3. **Install git**
 4. `cp ~/.shells/shellconfig.example ~/.shells/.shellconfig`
 5. Modify ~/.shells/.shellconfig
 6. Add
  `[ -f $HOME/.shells/.bashrc ] && source $HOME/.shells/.bashrc`
to ~/.zshrc or ~/.bashrc
 7. `source ~/.zshrc or ~/.bashrc`
